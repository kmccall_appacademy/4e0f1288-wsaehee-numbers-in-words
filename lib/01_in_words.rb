class Fixnum

  def in_words(num = nil)
    if num == nil
      return "zero" if self == 0
      @@num = self
    else
      @@num = num
    end

    stringify = ""

    words = {
        1000000000000 => "trillion", 1000000000 => "billion", 1000000 => "million", 1000 => "thousand", 100 => "hundred",
        90 => "ninety", 80 => "eighty", 70 => "seventy", 60 => "sixty", 50 => "fifty", 40 => "forty", 30 => "thirty", 20 => "twenty",
        19 => "nineteen", 18 => "eighteen", 17 => "seventeen", 16 => "sixteen", 15 => "fifteen", 14 => "fourteen", 13 => "thirteen", 12 => "twelve", 11 => "eleven",10 => "ten",
        9 => "nine", 8 => "eight", 7 => "seven", 6 => "six", 5 => "five", 4 => "four", 3 => "three", 2 => "two", 1 => "one"
    }

    words.each do |int, word|
      if @@num == 0
        return stringify.strip
      elsif(@@num < 21 && @@num == int) || (@@num < 100 && @@num == int)
        return stringify + word
      elsif @@num < 100 && @@num/int > 0
        return stringify + "#{word} " + in_words(@@num % int)
      elsif @@num > 99 && @@num % int == 0
        return stringify + in_words(@@num/int) + " #{word}"
      elsif @@num/int > 0
        last = @@num % int
        return stringify + in_words(@@num/int) + " #{word} " + in_words(last)
      end
    end
  end

end
